"""
Author: Marius-Danut Iancu
Email:  marius.danut94@outlook.com
Creation date (mm-dd-yyyy): 3/12/2018
Version: 0.1
Status: Production

A. I. CUZA UNIVERSITY OF IAŞI
Faculty of Computer Science Iasi
"""

import os
import unittest

from problems.pr1 import digit_sum
from problems.pr2 import character_occurence
from problems.pr3 import is_valid
from problems.pr4 import sort_emails


class Pr1(unittest.TestCase):

    def setUp(self):
        pass

    def test_163(self):
        test_input = 163
        expected_result = 10
        self.assertEqual(digit_sum(test_input), expected_result)

    def test_negative_16(self):
        test_input = -16
        expected_result = 7
        self.assertEqual(digit_sum(test_input), expected_result)

    def test_type_error_1(self):
        test_input = 1.2
        expected_result = TypeError
        with self.assertRaises(expected_result):
            digit_sum(test_input)

    def test_type_error_2(self):
        test_input = [1, 2]
        expected_result = TypeError
        with self.assertRaises(expected_result):
            digit_sum(test_input)


class Pr2(unittest.TestCase):

    def setUp(self):
        pass

    def test_string_assessment(self):
        test_input = 'assessment'
        expected_result = {'a': 1, 's': 4, 'e': 2, 'm': 1, 'n': 1, 't': 1}
        self.assertEqual(character_occurence(test_input), expected_result)


class Pr3(unittest.TestCase):

    def setUp(self):
        pass

    def test_exception(self):
        test_input_a = []
        test_input_b = []
        expected_result = ValueError
        with self.assertRaises(expected_result):
            is_valid(test_input_a, test_input_b)

    def test_TypeError(self):
        test_input_a = 1
        test_input_b = 2
        expected_result = TypeError
        with self.assertRaises(expected_result):
            is_valid(test_input_a, test_input_b)

    def test_1(self):
        test_input_a = [1, 2, 3]
        test_input_b = [7, 3]
        expected_result = True
        self.assertEqual(is_valid(test_input_a, test_input_b), expected_result)

    def test_2(self):
        test_input_a = [1, 2, 4]
        test_input_b = [2, 5]
        expected_result = False
        self.assertEqual(is_valid(test_input_a, test_input_b), expected_result)

    def test_3(self):
        test_input_a = [1]
        test_input_b = [1]
        expected_result = True
        self.assertEqual(is_valid(test_input_a, test_input_b), expected_result)

    def test_4(self):
        test_input_a = [1]
        test_input_b = [2]
        expected_result = False
        self.assertEqual(is_valid(test_input_a, test_input_b), expected_result)


class Pr4(unittest.TestCase):

    def setUp(self):
        self.current_directory = os.path.dirname(os.path.realpath(__file__))
        self.file_directory = os.path.normpath(os.path.join(self.current_directory, '..', 'files'))
        self.file_extension = ".txt"

    def test_1(self):
        input_file = os.path.normpath(os.path.join( self.file_directory, "input" + self.file_extension))
        expected_result = ['andrew@yahoo.com', 'Andrew@yahoo.com', 'danle@gmail.com', 'Danle@gmail.com',
                           'john_smith@gmail.com']
        self.assertEqual(sort_emails(input_file),  expected_result)

    def test_strings_FileNotFoundError(self):
        input_file = os.path.normpath(os.path.join(self.current_directory, "input" + self.file_extension))
        expected_result = FileNotFoundError
        with self.assertRaises(expected_result):
            sort_emails(input_file)


if __name__ == '__main__':
    unittest.main()

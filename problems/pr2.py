"""
Author: Marius-Danut Iancu
Email:  marius.danut94@outlook.com
Creation date (mm-dd-yyyy): 3/12/2018
Version: 0.1
Status: Production

A. I. CUZA UNIVERSITY OF IAŞI
Faculty of Computer Science Iasi
"""


def character_occurence(input_string):
    """
    A function which takes input from keyboard and returns a dictionary
    which contains how many times each character from the input appears.

    :param input_string: input data
    :return: a dictionary containing character occurrences
    """

    m_string = str(input_string)
    result = {}

    for character in m_string:
        result[character] = result.get(character, 0) + 1

    return result




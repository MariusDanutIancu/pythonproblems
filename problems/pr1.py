"""
Author: Marius-Danut Iancu
Email:  marius.danut94@outlook.com
Creation date (mm-dd-yyyy): 3/12/2018
Version: 0.1
Status: Production

A. I. CUZA UNIVERSITY OF IAŞI
Faculty of Computer Science Iasi
"""


def digit_sum(num):
    """
    A function which takes an integer as input and returns the sum of its digits.

    :param num: input integer
    :return: sum of num digits
    """

    if not isinstance(num, int):
        raise TypeError("Num is not an integer")

    result = 0
    num = abs(num)
    while num:
        result, num = result + num % 10, num // 10

    return result

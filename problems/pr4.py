"""
Author: Marius-Danut Iancu
Email:  marius.danut94@outlook.com
Creation date (mm-dd-yyyy): 3/12/2018
Version: 0.1
Status: Production

A. I. CUZA UNIVERSITY OF IAŞI
Faculty of Computer Science Iasi
"""


def sort_emails(input_file):
    """
    A function ​which will take the name of the file containing email
    addresses as parameter, will read from that file all the emails and will return the email
    addresses sorted alphabetically after the first letter.

    :param input_file: Input file path
    :return: A sorted list
    """

    with open(input_file, 'r') as file_obj:
        result = [line.rstrip() for line in file_obj]
    result.sort(key=lambda x: x[0].lower())
    return result




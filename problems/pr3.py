"""
Author: Marius-Danut Iancu
Email:  marius.danut94@outlook.com
Creation date (mm-dd-yyyy): 3/12/2018
Version: 0.1
Status: Production

A. I. CUZA UNIVERSITY OF IAŞI
Faculty of Computer Science Iasi
"""

FIRST = 0
LAST = -1


def is_valid(a, b):
    """
    A function ​which will return true​ if
    the lists have the same first element or they have the same last element, otherwise will
    return false​. Both arrays will be length 1 or more.

    :param a: first list
    :param b: second list
    :return: True if a[FIRST]==b[FIRST] OR a[LAST] == b[LAST], false otherwise
    """

    if not (isinstance(a, list) and isinstance(b, list)):
        raise TypeError("Input is not a list")

    if len(a) < 1 and len(a) < 1:
        raise ValueError("Input does not satisfy requirements")

    if not (a[FIRST] == b[FIRST] or a[LAST] == b[LAST]):
        return False
    return True
